from sqlalchemy import Column, Integer

from config import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, autoincrement=False, primary_key=True)
