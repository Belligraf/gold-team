from typing import AsyncGenerator

from environs import Env
from sqlalchemy import MetaData
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import declarative_base, sessionmaker
from loguru import logger
from kafka_producer import KafkaProducer

env = Env()
env.read_env()

AUTH_SECRET = env.str("AUTH_SECRET")

DB_USER = env.str("DB_USER")
DB_PASS = env.str("DB_PASS")
DB_HOST = env.str("DB_HOST")
DB_NAME = env.str("DB_NAME")
PORT = env.int("PORT")

DATABASE_URL = f"postgresql+asyncpg://{DB_USER}:{DB_PASS}@{DB_HOST}/{DB_NAME}"

try:
    engine = create_async_engine(DATABASE_URL, future=True, echo=True)
    async_session = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession, future=True)
    Base = declarative_base()
    metadata = MetaData()
except Exception:
    logger.critical(f"Не смог подключится к БД: {DATABASE_URL}")
    exit()

KAFKA_STATISTICS_TOPIC_NAME = env.str("KAFKA_STATISTICS_TOPIC_NAME")
KAFKA_ACCOUNT_TOPIC_NAME = env.str("KAFKA_ACCOUNT_TOPIC_NAME")
KAFKA_BOOTSTRAP_SERVER = env.str("KAFKA_BOOTSTRAP_SERVER")
kafka_producer = KafkaProducer(KAFKA_STATISTICS_TOPIC_NAME, KAFKA_BOOTSTRAP_SERVER)

async def get_async_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session() as session:
        yield session
