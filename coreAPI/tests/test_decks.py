import pytest
from httpx import AsyncClient


@pytest.mark.parametrize("input_data,expected_status,error",
                         [((1, 'Test Deck 1'), 201, ''), ((2, 'Test Deck 2'), 404, 'User with such id does not exist'),
                          (("fwa21", 'aWF'), 422, 'value is not a valid integer')])
@pytest.mark.asyncio
async def test_post_create_deck(ac: AsyncClient, input_data, expected_status, error):
    response = await ac.post("/decks", headers={"user-id": f'{input_data[0]}'}, json={"name": input_data[1]})
    assert response.status_code == expected_status
    if error and isinstance(response.json()['detail'], list):
        assert response.json()['detail'][0]['msg'] == error
    elif error:
        assert response.json()['detail'] == error
    else:
        assert response.json()['name'] == input_data[1]


@pytest.mark.asyncio
async def test_get_deck(ac: AsyncClient):
    response = await ac.get("/decks", headers={"user-id": "1"})
    assert response.status_code == 200
    assert response.json()[0]['name'] == "Test name"


@pytest.mark.parametrize("input_data,expected_status,error",
                         [
                             ((1, 2, "New Test Table name"), 404, "User with such id does not exist"),
                             ((15, 1, "New Test Table name"), 404, "Deck with such id does not exist"),
                             ((1, 1, "New Test Table name"), 204, '')
                         ])
@pytest.mark.asyncio
async def test_upgrade_deck(ac: AsyncClient, input_data, expected_status, error):
    response = await ac.patch(f"/decks/{input_data[0]}",
                              headers={"user-id": str(input_data[1]), "deck_id": str(input_data[0])},
                              json={"name": input_data[2]})
    assert response.status_code == expected_status
    if error:
        assert response.json()['detail'] == error


@pytest.mark.parametrize("input_data,expected_status,error",
                         [
                             ((1, 2), 404, "User with such id does not exist"),
                             ((15, 1), 404, "Deck with such id does not exist"),
                             ((1, 1), 204, '')
                         ])
@pytest.mark.asyncio
async def test_delete_deck(ac: AsyncClient, input_data, expected_status, error):
    response = await ac.delete(f"/decks/{input_data[0]}",
                               headers={"user-id": str(input_data[1]), "deck_id": str(input_data[0])})
    assert response.status_code == expected_status
    if error:
        assert response.json()['detail'] == error
