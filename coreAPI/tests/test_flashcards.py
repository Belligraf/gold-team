import pytest
from httpx import AsyncClient


@pytest.mark.parametrize("input_data,expected_status,error",
                         [
                             ((13, 'Test Card 1', 'Test Card 1', 'Test Card 1'), 201, ''),
                             ((13, 'Test Card 2', 'Test Card 2', 'Test Card 2'), 201, ''),
                             ((14, 'Test Card 2', 'Test Card 2', 'Test Card 2'), 404,
                              "Deck with such id does not exist")
                         ])
@pytest.mark.asyncio
async def test_post_create_flashcards(ac: AsyncClient, input_data, expected_status, error):
    response = await ac.post("/flashcards", headers={"user-id": "1"}, json={
        "front_side": input_data[1],
        "back_side": input_data[2],
        "comment": input_data[3],
        "deck_id": input_data[0],
        "is_shared_deck_flashcard": False
    })
    assert response.status_code == expected_status
    if error:
        assert response.json()['detail'] == error
    else:
        assert response.json()['front_side'] == input_data[1]


@pytest.mark.parametrize("input_data,expected_status,error",
                         [
                             ((13, 'Test Card 1', 'Test Card 1', 'Test Card 1', 1), 204, ''),
                             ((13, 'Test Card 2', 'Test Card 2', 'Test Card 2', 23), 404,
                              "Flashcard with such id does not exist")
                         ])
@pytest.mark.asyncio
async def test_update_flashcards(ac: AsyncClient, input_data, expected_status, error):
    response = await ac.patch(f"/flashcards/{str(input_data[4])}",
                              headers={"user-id": "1", "flashcard_id": str(input_data[4])}, json={
            "front_side": input_data[1],
            "back_side": input_data[2],
            "comment": input_data[3],
            "deck_id": input_data[0],
        })
    assert response.status_code == expected_status
    if error:
        assert response.json()["detail"] == error


@pytest.mark.parametrize("input_data,expected_status,error",
                         [
                             ({"deck_id": "13"}, 200, ''),
                             ({"deck_id": "13", "shared_deck_id": "1"}, 400,
                              "Don't provide both deck_id and shared_deck_id query parameters. Strictly one of them is required."),
                             ({"deck_id": "137"}, 404,
                              "Deck with such id does not exist")
                         ])
@pytest.mark.asyncio
async def test_get_flashcards(ac: AsyncClient, input_data, expected_status, error):
    response = await ac.get("/flashcards", headers={"user-id": "1"}, params=input_data)

    assert response.status_code == expected_status
    if error:
        assert response.json()['detail'] == error
    else:
        assert response.json()[0]["front_side"] == "Test Card 1"


@pytest.mark.parametrize("input_data,expected_status,error",
                         [
                             ({
                                  "deck_id": 13,
                                  "flashcard_id": 2,
                                  "record": "remember",
                                  "user-id": "1"
                              }, 204, ''),
                             ({
                                  "deck_id": 13,
                                  "flashcard_id": 2,
                                  "record": "remember",
                                  "user-id": "45"
                              }, 404,
                              "User with such id does not exist"),
                             ({
                                  "deck_id": 124,
                                  "flashcard_id": 2,
                                  "record": "remember",
                                  "user-id": "1"
                              }, 404,
                              "Deck with such id does not exist"),
                             ({
                                  "deck_id": 13,
                                  "flashcard_id": 212,
                                  "record": "remember",
                                  "user-id": "1"
                              }, 404,
                              "Flashcard with such id does not exist"),
                             ({
                                  "deck_id": 13,
                                  "flashcard_id": 2,
                                  "record": "Noremember",
                                  "user-id": "1"
                              }, 422,
                              "Invalid 'record' field value")

                         ])
@pytest.mark.asyncio
async def test_create_study_record(ac: AsyncClient, input_data, expected_status, error):
    response = await ac.post("/decks/study", headers={"user-id": input_data["user-id"]}, json=input_data)
    assert response.status_code == expected_status
    if error:
        assert response.json()['detail'] == error


@pytest.mark.parametrize("input_data,expected_status,error",
                         [
                             ({"deck_id": "13", "user-id": "1"}, 200, ''),
                             ({"deck_id": "1123", "user-id": "1"}, 404,
                              "Deck with such id does not exist"),
                             ({"deck_id": "13", "user-id": "121"}, 404,
                              "User with such id does not exist")
                         ])
@pytest.mark.asyncio
async def test_get_study_record(ac: AsyncClient, input_data, expected_status, error):
    response = await ac.get("/decks/study", headers=input_data, params=input_data)
    assert response.status_code == expected_status
    if error:
        assert response.json()['detail'] == error
    else:
        assert response.json()[0]["front_side"] == "Test Card 1"


@pytest.mark.parametrize("input_data,expected_status,error",
                         [
                             ((1, 'Test Card 1', 'Test Card 1', 'Test Card 1', 1), 204, ''),
                             ((15, 'Test Card 2', 'Test Card 2', 'Test Card 2', 1), 404,
                              "Flashcard with such id does not exist")
                         ])
@pytest.mark.asyncio
async def test_delete_flashcards(ac: AsyncClient, input_data, expected_status, error):
    response = await ac.delete(f"/flashcards/{input_data[4]}",
                               headers={"user-id": str(input_data[4]), "flashcard_id": str(input_data[0])})
    assert response.status_code == expected_status
    if error:
        assert response.json()["detail"] == error
