import pytest
from httpx import AsyncClient


@pytest.mark.parametrize("input_data,expected_status,error",
                         [((1, 'Test Shared_Deck 1', 'Test desc 1'), 201, ''),
                          ((2, 'Test Shared_Deck 2', 'Test desc 2'), 404, 'User with such id does not exist'),
                          (("fwa21", 'aWF', 'Test desc 3'), 422, 'value is not a valid integer')])
@pytest.mark.asyncio
async def test_post_create_shared_deck(ac: AsyncClient, input_data, expected_status, error):
    response = await ac.post("/shared_decks", headers={"user-id": f'{input_data[0]}'},
                             json={"name": input_data[1], "description": input_data[2]})
    assert response.status_code == expected_status
    if error and isinstance(response.json()['detail'], list):
        assert response.json()['detail'][0]['msg'] == error
    elif error:
        assert response.json()['detail'] == error
    else:
        assert response.json()['name'] == input_data[1]


@pytest.mark.parametrize("input_data,expected_status,error",
                         [((1, "Test Shared_Deck 1"), 200, ''),
                          ((2, 'Test desc 2'), 404, 'Deck with such id does not exist')])
@pytest.mark.asyncio
async def test_get_shared_deck(ac: AsyncClient, input_data, expected_status, error):
    response = await ac.get(f"/shared_decks/{input_data[0]}", headers={"shared_deck_id": str(input_data[0])})
    assert response.status_code == expected_status
    if error:
        assert response.json()['detail'] == error
    else:
        assert response.json()['name'] == input_data[1]


@pytest.mark.parametrize("input_data,expected_status,error",
                         [((1, "Test Shared_Deck 1"), 200, '')])
@pytest.mark.asyncio
async def test_get_shared_deck(ac: AsyncClient, input_data, expected_status, error):
    response = await ac.get(f"/shared_decks", headers={"user-id": str(input_data[0])})
    assert response.status_code == expected_status
    assert response.json()[0]['name'] == input_data[1]


@pytest.mark.parametrize("input_data,expected_status,error",
                         [
                             ((1, 2), 404, "User with such id does not exist"),
                             ((15, 1), 404, "Deck with such id does not exist"),
                             ((1, 1), 201, ''),
                             (("fq", 1), 422, "value is not a valid integer"),
                         ])
@pytest.mark.asyncio
async def test_post_deck_from_shared(ac: AsyncClient, input_data, expected_status, error):
    response = await ac.post(f"/decks/from_shared_deck",
                             headers={"user-id": str(input_data[1])}, params={"shared_deck_id": str(input_data[0])})
    assert response.status_code == expected_status
    if error and isinstance(response.json()['detail'], list):
        assert response.json()['detail'][0]['msg'] == error
    elif error:
        assert response.json()['detail'] == error


@pytest.mark.parametrize("input_data,expected_status,error",
                         [
                             ((15, 1, "New Test Table name", "Test desc"), 404, "Deck with such id does not exist"),
                             ((1, 1, "New Test Table name", "Test desc"), 204, '')
                         ])
@pytest.mark.asyncio
async def test_upgrade_shared_deck(ac: AsyncClient, input_data, expected_status, error):
    response = await ac.patch(f"/shared_decks/{input_data[0]}",
                              headers={"user-id": str(input_data[1]), "deck_id": str(input_data[0])},
                              json={"name": input_data[2], "description": input_data[3]})
    assert response.status_code == expected_status
    if error:
        assert response.json()['detail'] == error


@pytest.mark.parametrize("input_data,expected_status,error",
                         [
                             ((1, 2, "true", "New Test Table name"), 404, "User with such id does not exist"),
                             ((15, 1, "true", "New Test Table name"), 404, "Deck with such id does not exist"),
                             ((1, 1, "true", "New Test Table name"), 204, ''),
                             (
                                     (1, 1, "not valid", "New Test Table name"), 422,
                                     "value could not be parsed to a boolean"),
                         ])
@pytest.mark.asyncio
async def test_post_deck_review(ac: AsyncClient, input_data, expected_status, error):
    response = await ac.post(f"/shared_decks/review",
                             headers={"user-id": str(input_data[1])},
                             json={"comment": input_data[3], "deck_id": str(input_data[0]),
                                   "is_positive": input_data[2]})
    assert response.status_code == expected_status
    if error and isinstance(response.json()['detail'], list):
        assert response.json()['detail'][0]['msg'] == error
    elif error:
        assert response.json()['detail'] == error


@pytest.mark.parametrize("input_data,expected_status,error",
                         [
                             ((15, 1), 404, "Deck with such id does not exist"),
                             ((1, 1), 204, '')
                         ])
@pytest.mark.asyncio
async def test_delete_shared_deck(ac: AsyncClient, input_data, expected_status, error):
    response = await ac.delete(f"/shared_decks/{input_data[0]}",
                               headers={"user-id": str(input_data[1]), "deck_id": str(input_data[0])})
    assert response.status_code == expected_status
