import asyncio
from typing import AsyncGenerator

import pytest_asyncio
from fastapi.testclient import TestClient
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.pool import NullPool
from httpx import AsyncClient
from sqlalchemy.orm import sessionmaker

from common_models import User

from main import app
from config import Base
from config import metadata
from config import get_async_session

from decks.models import Deck

DATABASE_URL_TEST = f"postgresql+asyncpg://postgres:postgres@localhost:5432/postgres"

engine_test = create_async_engine(DATABASE_URL_TEST, poolclass=NullPool, )
async_session_maker = sessionmaker(engine_test, class_=AsyncSession, expire_on_commit=False)
metadata.bind = engine_test


@pytest_asyncio.fixture()
async def override_get_async_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session_maker() as session:
        yield session


app.dependency_overrides[get_async_session] = override_get_async_session


@pytest_asyncio.fixture(autouse=True, scope='session')
async def prepare_database():
    async with engine_test.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    yield
    async with engine_test.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)


@pytest_asyncio.fixture()
async def create_model(override_get_async_session):
    user = User(id=1)
    deck = Deck(id=13, user_id=1, name="Test name")
    override_get_async_session.add(user)
    override_get_async_session.add(deck)
    await override_get_async_session.commit()


# SETUP
@pytest_asyncio.fixture(scope='session')
def event_loop(request):
    """Create an instance of the default event loop for each test case."""
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


client = TestClient(app)


@pytest_asyncio.fixture(scope="session")
async def ac() -> AsyncClient:
    async with AsyncClient(app=app, base_url="http://0.0.0.0") as ac:
        yield ac
