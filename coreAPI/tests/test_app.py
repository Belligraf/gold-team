from tests.conftest import client

def test_main_page(create_model):
    response = client.get("/docs")
    assert response.status_code == 200
