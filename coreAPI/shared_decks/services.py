class DeckNotExistsException(Exception):
    pass


class UserNotExistsException(Exception):
    pass
