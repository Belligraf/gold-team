from datetime import datetime

from pydantic import BaseModel, Field


class SharedDeckBase(BaseModel):
    name: str
    description: str


class SharedDeckResponse(SharedDeckBase):
    id: int
    created_at: datetime
    updated_at: datetime

    class Config:
        orm_mode = True


class SharedDeckReviewResponse(BaseModel):
    is_positive: bool
    comment: str | None

    class Config:
        orm_mode = True


class SharedDeckFullResponse(SharedDeckResponse):
    reviews: list[SharedDeckReviewResponse]

    class Config:
        orm_mode = False


class SharedDeckReviewCreate(BaseModel):
    deck_id: int
    is_positive: bool = Field(..., description="True indicates like; False indicates dislike")
    comment: str
