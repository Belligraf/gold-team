from datetime import datetime

from sqlalchemy import update, delete
from sqlalchemy.future import select
from sqlalchemy.orm import Session

from common_models import User
from config import async_session
from shared_decks import SharedDeck, SharedDeckReview
from shared_decks.schemas import SharedDeckFullResponse, SharedDeckReviewCreate
from shared_decks.services import UserNotExistsException, DeckNotExistsException


class SharedDecksDAL:
    def __init__(self, db_session: Session):
        self.db_session = db_session

    async def _get_all_decks_ids(self) -> list[int]:
        result = await self.db_session.execute(select(SharedDeck.id))
        return result.scalars().all()

    async def _get_all_decks_ids_for_user(self, user_id: int) -> list[int]:
        result = await self.db_session.execute(select(SharedDeck.id).where(SharedDeck.user_id == user_id))
        return result.scalars().all()

    async def _get_all_users_ids(self) -> list[int]:
        result = await self.db_session.execute(select(User.id))
        return result.scalars().all()

    async def get_decks(self, user_id: int | None, name: str | None) -> list[SharedDeck]:
        request = select(SharedDeck)
        if user_id:
            request = request.where(SharedDeck.user_id == user_id)
        if name:
            request = request.where(SharedDeck.name.ilike(f"%{name}%"))
        request = request.order_by(SharedDeck.created_at)
        result = await self.db_session.execute(request)
        return result.scalars().all()

    async def get_deck(self, deck_id: int) -> SharedDeckFullResponse | None:
        result = await self.db_session.execute(
            select(SharedDeck).where(SharedDeck.id == deck_id)
        )
        deck = result.scalars().first()
        if deck is None:
            return None
        result = await self.db_session.execute(
            select(SharedDeckReview).where(SharedDeckReview.deck_id == deck_id)
        )
        reviews = result.scalars().all()
        return SharedDeckFullResponse(
            name=deck.name,
            description=deck.description,
            id=deck.id,
            created_at=deck.created_at,
            updated_at=deck.updated_at,
            reviews=reviews
        )

    async def create_deck(self, user_id: int, name: str, description: str) -> SharedDeck:
        if user_id not in await self._get_all_users_ids():
            raise UserNotExistsException()

        deck = SharedDeck(user_id=user_id, name=name, description=description)
        self.db_session.add(deck)
        await self.db_session.flush()
        await self.db_session.refresh(deck)
        return deck

    async def create_deck_review(self, review: SharedDeckReviewCreate, user_id: int):
        if user_id not in await self._get_all_users_ids():
            raise UserNotExistsException()
        if review.deck_id not in await self._get_all_decks_ids():
            raise DeckNotExistsException()

        result = await self.db_session.execute(
            select(
                SharedDeckReview
            ).where(
                SharedDeckReview.user_id == user_id
            ).where(
                SharedDeckReview.deck_id == review.deck_id
            )
        )
        old_review = result.scalars().first()
        if old_review:
            old_review.is_positive = review.is_positive
            old_review.comment = review.comment
        else:
            new_review = SharedDeckReview(
                user_id=user_id,
                deck_id=review.deck_id,
                is_positive=review.is_positive,
                comment=review.comment
            )
            self.db_session.add(new_review)
        await self.db_session.flush()

    async def update_deck(self, user_id: int, deck_id: int, new_name: str, new_description: str) -> bool:
        if deck_id not in await self._get_all_decks_ids_for_user(user_id):
            return False

        await self.db_session.execute(
            update(
                SharedDeck
            ).where(
                SharedDeck.id == deck_id
            ).values(
                name=new_name,
                description=new_description,
                updated_at=datetime.now()
            )
        )
        await self.db_session.flush()
        return True

    async def delete_deck(self, deck_id: int, user_id: int) -> bool:
        if deck_id not in await self._get_all_decks_ids_for_user(user_id):
            return False

        await self.db_session.execute(
            delete(SharedDeck).where(SharedDeck.id == deck_id)
        )
        await self.db_session.flush()
        return True


async def get_shared_decks_dal():
    async with async_session() as session:
        async with session.begin():
            yield SharedDecksDAL(session)
