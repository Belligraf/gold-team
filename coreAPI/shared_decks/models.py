from sqlalchemy import Column, Integer, ForeignKey, String, Text, DateTime, Boolean, func

from config import Base


class SharedDeck(Base):
    __tablename__ = "shared_decks"

    id = Column(Integer, autoincrement=True, primary_key=True)
    user_id = Column(Integer, ForeignKey("users.id", ondelete="CASCADE"), autoincrement=False)
    name = Column(String(150), nullable=False)
    description = Column(Text)
    created_at = Column(DateTime, nullable=False, server_default=func.now())
    updated_at = Column(DateTime, nullable=False, server_default=func.now())


class SharedDeckReview(Base):
    __tablename__ = "shared_decks_reviews"

    user_id = Column(Integer, ForeignKey("users.id", ondelete="CASCADE"), primary_key=True, autoincrement=False)
    deck_id = Column(Integer, ForeignKey("shared_decks.id", ondelete="CASCADE"), primary_key=True, autoincrement=False)
    is_positive = Column(Boolean, nullable=False)  # если True, значит это лайк; если False - дизлайк
    comment = Column(Text)
