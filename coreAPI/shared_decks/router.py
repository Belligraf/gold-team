from fastapi import APIRouter, status, Depends, HTTPException, Response, Header
from loguru import logger
from shared_decks.dal import SharedDecksDAL, get_shared_decks_dal
from shared_decks.schemas import SharedDeckBase, SharedDeckResponse, SharedDeckFullResponse, SharedDeckReviewCreate
from shared_decks.services import UserNotExistsException, DeckNotExistsException

router = APIRouter(prefix="/shared_decks", tags=["Shared decks"])

common_responses = {
    status.HTTP_404_NOT_FOUND: {
        "content": {
            "application/json": {
                "example": {
                    "detail": "Deck with such id does not exist"
                }
            }
        }
    }
}
post_responses = {
    status.HTTP_404_NOT_FOUND: {
        "content": {
            "application/json": {
                "example": {
                    "detail": "User with such id does not exist"
                }
            }
        }
    }
}
both_responses = {
    status.HTTP_404_NOT_FOUND: {
        "content": {
            "application/json": {
                "examples": {
                    "User does not exist": {
                        "value": {
                            "detail": "User with such id does not exist"
                        }
                    },
                    "Deck does not exist": {
                        "value": {
                            "detail": "Deck with such id does not exist"
                        }
                    }
                }
            }
        }
    }
}


@router.get("", response_model=list[SharedDeckResponse])
async def get_decks(
        name: str | None = None,
        author_user_id: int | None = None,
        decks_dal: SharedDecksDAL = Depends(get_shared_decks_dal)
):
    """
    Получить список общих колод
    """
    logger.info(f"Получение списка общих колод {author_user_id}:{name}")
    return await decks_dal.get_decks(author_user_id, name)


@router.get("/{shared_deck_id:int}", response_model=SharedDeckFullResponse, responses=common_responses)
async def get_deck(shared_deck_id: int, decks_dal: SharedDecksDAL = Depends(get_shared_decks_dal)):
    """
    Получить конкретную доску
    """
    deck = await decks_dal.get_deck(shared_deck_id)
    if deck:
        logger.success(f"Вывод колоды {shared_deck_id}")
        return deck
    else:
        logger.error(f"Не удалось получить колоду {shared_deck_id}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Deck with such id does not exist")


@router.post("", response_model=SharedDeckResponse, status_code=status.HTTP_201_CREATED, responses=post_responses)
async def create_deck(
        deck: SharedDeckBase,
        user_id: int = Header(...),
        decks_dal: SharedDecksDAL = Depends(get_shared_decks_dal)
):
    """
    Создание общей колоды
    """
    try:
        new_deck = await decks_dal.create_deck(user_id, deck.name, deck.description)
    except UserNotExistsException:
        logger.error(f"Не удалось создать новую колоду deck:{deck}, user_id:{user_id}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "User with such id does not exist")
    else:
        logger.success(f"Создана новая колода deck:{deck.name}, user_id:{user_id}")
        return new_deck


@router.post("/review", status_code=status.HTTP_204_NO_CONTENT, responses=both_responses)
async def create_review(
        review: SharedDeckReviewCreate,
        user_id: int = Header(...),
        decks_dal: SharedDecksDAL = Depends(get_shared_decks_dal)
):
    """
    Отзыв о колоде
    """
    try:
        await decks_dal.create_deck_review(review, user_id)
    except UserNotExistsException as e:
        logger.error(f"Не удалось создать review, user_id:{user_id}, review:{review} {e}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "User with such id does not exist")
    except DeckNotExistsException as e:
        logger.error(f"Не удалось создать review, user_id:{user_id}, review:{review} {e}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Deck with such id does not exist")
    else:
        logger.success(f"Создан новый reviews user_id:{user_id}, review:{review}")
        return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.patch("/{deck_id:int}", status_code=status.HTTP_204_NO_CONTENT, responses=common_responses)
async def update_deck(
        deck_id: int,
        deck: SharedDeckBase,
        user_id: int = Header(...),
        decks_dal: SharedDecksDAL = Depends(get_shared_decks_dal)
):
    """
    Обновить общую колоду
    """
    deck_was_updated = await decks_dal.update_deck(user_id, deck_id, deck.name, deck.description)
    if deck_was_updated:
        logger.success(f"Колода {deck_id} обновлена user_id:{user_id}, deck:{deck}")
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    else:
        logger.error(f"Не удалось обносить колоду {deck_id} user_id:{user_id}, deck:{deck}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Deck with such id does not exist")


@router.delete("/{deck_id:int}", status_code=status.HTTP_204_NO_CONTENT, responses=common_responses)
async def delete_deck(
        deck_id: int,
        user_id: int = Header(...),
        decks_dal: SharedDecksDAL = Depends(get_shared_decks_dal)
):
    """
    Удаление общей колоды
    """
    deck_was_deleted = await decks_dal.delete_deck(deck_id, user_id)
    if deck_was_deleted:
        logger.success(f"Общая колода удалена deck_id:{deck_id}, user_id:{user_id}")
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    else:
        logger.error(f"Не смог удалить общую колоду с deck_id:{deck_id}, user_id:{user_id}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Deck with such id does not exist")
