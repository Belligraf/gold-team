from sqlalchemy.orm import Session

from common_models import User


class CommonModelsDAL:
    def __init__(self, db_session: Session):
        self.db_session = db_session

    async def create_user(self, user_id: int):
        new_user = User(id=user_id)
        self.db_session.add(new_user)
        try:
            await self.db_session.commit()
        except Exception:
            pass
