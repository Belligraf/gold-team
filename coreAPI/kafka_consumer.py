from aiokafka import AIOKafkaConsumer

from common_models_dal import CommonModelsDAL
from config import async_session
from proto.user_pb2 import UserCreatedMessage
from loguru import logger


class KafkaConsumer:
    def __init__(self, topic: str, host: str):
        self.topic = topic
        self.host = host
        self.consumer: AIOKafkaConsumer | None = None
        self.common_models_dal: CommonModelsDAL | None = None

    async def _init_services(self):
        logger.info(f"Инициализирую сервис kafka consumer")
        if self.consumer is None:
            self.consumer = AIOKafkaConsumer(self.topic, bootstrap_servers=self.host)
        if self.common_models_dal is None:
            async with async_session() as session:
                async with session.begin():
                    self.common_models_dal = CommonModelsDAL(session)

    async def start(self):
        logger.info(f"Стартую - kafka consumer")
        await self._init_services()
        await self.consumer.start()
        try:
            async for msg in self.consumer:
                message = UserCreatedMessage()
                message.ParseFromString(msg.value)
                await self.common_models_dal.create_user(int(message.externalID))
        except Exception as e:
            logger.critical(f"Не смог запустить kafka consumer - {e}")
        finally:
            await self.consumer.stop()

    async def stop(self):
        logger.info(f"Останавливаю - kafka consumer")
        if self.consumer is not None:
            await self.consumer.stop()
