from datetime import datetime, timedelta

from sqlalchemy import update, delete
from sqlalchemy.future import select
from sqlalchemy.orm import Session

from common_models import User
from config import async_session, kafka_producer
from decks import Deck
from decks.schemas import StudyRecord
from decks.services import UserNotExistsException, DeckNotExistsException, FlashcardNotExistsException, \
    InvalidArgumentException
from flashcards import Flashcard, DeckFlashcard, SharedDeckFlashcard
from shared_decks import SharedDeck


class DecksDAL:
    def __init__(self, db_session: Session):
        self.db_session = db_session

    async def _get_all_decks_ids(self) -> list[int]:
        result = await self.db_session.execute(select(Deck.id))
        return result.scalars().all()

    async def _get_all_shared_decks_ids(self) -> list[int]:
        result = await self.db_session.execute(select(SharedDeck.id))
        return result.scalars().all()

    async def _get_all_users_ids(self) -> list[int]:
        result = await self.db_session.execute(select(User.id))
        return result.scalars().all()

    async def _get_all_flashcards_ids(self) -> list[int]:
        result = await self.db_session.execute(select(Flashcard.id))
        return result.scalars().all()

    async def get_study_flashcards(self, user_id: int, deck_id: int) -> list[Flashcard]:
        if user_id not in await self._get_all_users_ids():
            raise UserNotExistsException()
        if deck_id not in await self._get_all_decks_ids():
            raise DeckNotExistsException()

        now = datetime.now()
        result = await self.db_session.execute(
            select(
                Flashcard
            ).join(
                DeckFlashcard
            ).join(
                Deck
            ).where(
                Deck.id == deck_id
            ).where(
                Deck.user_id == user_id
            ).where(
                Flashcard.next_appearance <= now
            )
        )
        return result.scalars().all()

    async def create_study_record(self, record: StudyRecord, user_id: int):
        if user_id not in await self._get_all_users_ids():
            raise UserNotExistsException()
        if record.deck_id not in await self._get_all_decks_ids():
            raise DeckNotExistsException()
        if record.flashcard_id not in await self._get_all_flashcards_ids():
            raise FlashcardNotExistsException()

        result = await self.db_session.execute(
            select(
                Flashcard
            ).join(
                DeckFlashcard
            ).join(
                Deck
            ).where(
                Deck.id == record.deck_id
            ).where(
                Deck.user_id == user_id
            ).where(
                Flashcard.id == record.flashcard_id
            )
        )
        flashcard = result.scalars().first()
        if record.record == "remember":
            if flashcard.level < 7:
                flashcard.level += 1
        elif record.record == "forget":
            if flashcard.level > 0:
                flashcard.level -= 1
        else:
            raise InvalidArgumentException()
        timestamp = record.timestamp or datetime.now()

        await kafka_producer.send_statistics_message(user_id, record.deck_id, record.flashcard_id, record.record,
                                                     timestamp)

        flashcard.next_appearance = timestamp + timedelta(hours=12) * 2 ** flashcard.level
        await self.db_session.flush()

    async def get_decks(self, user_id: int) -> list[Deck]:
        result = await self.db_session.execute(
            select(
                Deck
            ).where(
                Deck.user_id == user_id
            ).order_by(
                Deck.created_at
            )
        )
        return result.scalars().all()

    async def create_deck(self, user_id: int, name: str) -> Deck:
        if user_id not in await self._get_all_users_ids():
            raise UserNotExistsException()

        deck = Deck(user_id=user_id, name=name)
        self.db_session.add(deck)
        await self.db_session.flush()
        await self.db_session.refresh(deck)
        return deck

    async def create_deck_from_shared(self, shared_deck_id: int, user_id: int) -> Deck:
        if user_id not in await self._get_all_users_ids():
            raise UserNotExistsException()
        if shared_deck_id not in await self._get_all_shared_decks_ids():
            raise DeckNotExistsException()

        result = await self.db_session.execute(
            select(SharedDeck).where(SharedDeck.id == shared_deck_id)
        )
        shared_deck = result.scalars().first()
        new_deck = Deck(user_id=user_id, name=shared_deck.name)
        self.db_session.add(new_deck)
        await self.db_session.flush()
        await self.db_session.refresh(new_deck)
        result = await self.db_session.execute(
            select(SharedDeckFlashcard.flashcard_id).where(SharedDeckFlashcard.deck_id == shared_deck_id)
        )
        flashcards_ids = result.scalars().all()
        result = await self.db_session.execute(
            select(Flashcard).where(Flashcard.id.in_(flashcards_ids))
        )
        flashcards = result.scalars().all()
        for flashcard in flashcards:
            new_flashcard = Flashcard(
                front_side=flashcard.front_side,
                back_side=flashcard.back_side,
                comment=flashcard.comment
            )
            self.db_session.add(new_flashcard)
            await self.db_session.flush()
            await self.db_session.refresh(new_flashcard)
            new_deck_flashcard = DeckFlashcard(deck_id=new_deck.id, flashcard_id=new_flashcard.id)
            self.db_session.add(new_deck_flashcard)
        await self.db_session.flush()
        return new_deck

    async def update_deck(self, deck_id: int, user_id: int, new_name: str):
        if user_id not in await self._get_all_users_ids():
            raise UserNotExistsException()
        if deck_id not in await self._get_all_decks_ids():
            raise DeckNotExistsException()

        await self.db_session.execute(
            update(
                Deck
            ).where(
                Deck.id == deck_id
            ).where(
                Deck.user_id == user_id
            ).values(
                name=new_name
            )
        )
        await self.db_session.flush()

    async def delete_deck(self, deck_id: int, user_id: int):
        if user_id not in await self._get_all_users_ids():
            raise UserNotExistsException()
        if deck_id not in await self._get_all_decks_ids():
            raise DeckNotExistsException()

        await self.db_session.execute(
            delete(Deck).where(Deck.id == deck_id)
        )
        await self.db_session.flush()


async def get_decks_dal():
    async with async_session() as session:
        async with session.begin():
            yield DecksDAL(session)
