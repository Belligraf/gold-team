class UserNotExistsException(Exception):
    pass


class DeckNotExistsException(Exception):
    pass


class FlashcardNotExistsException(Exception):
    pass


class InvalidArgumentException(Exception):
    pass
