from loguru import logger
from fastapi import APIRouter, Depends, HTTPException, status, Response, Header

from decks.dal import DecksDAL, get_decks_dal
from decks.schemas import DeckResponse, DeckName, StudyFlashcard, StudyRecord
from decks.services import UserNotExistsException, DeckNotExistsException, FlashcardNotExistsException, \
    InvalidArgumentException

router = APIRouter(prefix="/decks", tags=["Decks"])

study_responses = {
    status.HTTP_404_NOT_FOUND: {
        "content": {
            "application/json": {
                "examples": {
                    "User does not exist": {
                        "value": {
                            "detail": "User with such id does not exist"
                        }
                    },
                    "Deck does not exist": {
                        "value": {
                            "detail": "Deck with such id does not exist"
                        }
                    },
                    "Flashcard does not exist": {
                        "value": {
                            "detail": "Flashcard with such id does not exist"
                        }
                    }
                }
            }
        }
    }
}
post_responses = {
    status.HTTP_404_NOT_FOUND: {
        "content": {
            "application/json": {
                "example": {
                    "detail": "User with such id does not exist"
                }
            }
        }
    }
}
user_deck_responses = {
    status.HTTP_404_NOT_FOUND: {
        "content": {
            "application/json": {
                "examples": {
                    "User does not exist": {
                        "value": {
                            "detail": "User with such id does not exist"
                        }
                    },
                    "Deck does not exist": {
                        "value": {
                            "detail": "Deck with such id does not exist"
                        }
                    }
                }
            }
        }
    }
}


@router.get("/study", response_model=list[StudyFlashcard], responses=user_deck_responses)
async def get_study_flashcards(deck_id: int, user_id: int = Header(...), decks_dal: DecksDAL = Depends(get_decks_dal)):
    """
    Получить список карточек для заучивания
    """
    try:
        flashcards = await decks_dal.get_study_flashcards(user_id, deck_id)
    except UserNotExistsException as e:
        logger.error(f"Не удалось получить список карточек для заучивания user_id:{user_id}, deck_id:{deck_id} {e}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "User with such id does not exist")
    except DeckNotExistsException as e:
        logger.error(f"Не удалось получить список карточек для заучивания user_id:{user_id}, deck_id:{deck_id} {e}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Deck with such id does not exist")
    else:
        logger.success(f"Получил список карточек для заучивания user_id:{user_id}, deck_id:{deck_id}")
        return flashcards


@router.post("/study", status_code=status.HTTP_204_NO_CONTENT, responses=study_responses)
async def create_study_record(
        record: StudyRecord,
        user_id: int = Header(...),
        decks_dal: DecksDAL = Depends(get_decks_dal)
):
    """
    Создать учебную запись
    """
    try:
        await decks_dal.create_study_record(record, user_id)
    except UserNotExistsException as e:
        logger.error(f"Не удалось создать учебную запись user_id:{user_id}, record:{record} {e}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "User with such id does not exist")
    except DeckNotExistsException as e:
        logger.error(f"Не удалось создать учебную запись user_id:{user_id}, record:{record} {e}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Deck with such id does not exist")
    except FlashcardNotExistsException as e:
        logger.error(f"Не удалось создать учебную запись user_id:{user_id}, record:{record} {e}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Flashcard with such id does not exist")
    except InvalidArgumentException as e:
        logger.error(f"Не удалось создать учебную запись user_id:{user_id}, record:{record} {e}")
        raise HTTPException(status.HTTP_422_UNPROCESSABLE_ENTITY, "Invalid 'record' field value")
    else:
        logger.success(f"Создал учебную запись user_id:{user_id}, record:{record}")
        return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.get("", response_model=list[DeckResponse])
async def get_decks(user_id: int = Header(...), decks_dal: DecksDAL = Depends(get_decks_dal)):
    """
    Получить список колод
    """
    logger.info(f"Получение списка колод user_id:{user_id}")
    return await decks_dal.get_decks(user_id)


@router.post("", response_model=DeckResponse, status_code=status.HTTP_201_CREATED, responses=post_responses)
async def create_deck(deck: DeckName, user_id: int = Header(...), decks_dal: DecksDAL = Depends(get_decks_dal)):
    """
    Создать новую колоду
    """
    try:
        new_deck = await decks_dal.create_deck(user_id, deck.name)
    except UserNotExistsException as e:
        logger.error(f"Не удалось создать колоду user_id:{user_id}, deck:{deck.name} {e}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "User with such id does not exist")
    else:
        logger.success(f"Создал новую колоду user_id:{user_id}, new_deck:{new_deck}")
        return new_deck


@router.post("/from_shared_deck", response_model=DeckResponse,
             status_code=status.HTTP_201_CREATED, responses=user_deck_responses)
async def create_deck_from_shared(
        shared_deck_id: int,
        user_id: int = Header(...),
        decks_dal: DecksDAL = Depends(get_decks_dal)
):
    """
    Создать новую общую колоду
    """
    try:
        new_deck = await decks_dal.create_deck_from_shared(shared_deck_id, user_id)
    except UserNotExistsException as e:
        logger.error(f"Не удалось создать общую колоду user_id:{user_id}, shared_deck_id:{shared_deck_id} {e}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "User with such id does not exist")
    except DeckNotExistsException as e:
        logger.error(f"Не удалось создать общую колоду user_id:{user_id}, shared_deck_id:{shared_deck_id} {e}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Deck with such id does not exist")
    else:
        logger.success(f"Создал общую колоду user_id:{user_id}, shared_deck_id:{shared_deck_id} {new_deck}")
        return new_deck


@router.patch("/{deck_id:int}", status_code=status.HTTP_204_NO_CONTENT, responses=user_deck_responses)
async def update_deck(
        deck_id: int,
        deck: DeckName,
        user_id: int = Header(...),
        decks_dal: DecksDAL = Depends(get_decks_dal)
):
    """
    Обновление (изменение) колоды
    """
    try:
        await decks_dal.update_deck(deck_id, user_id, deck.name)
    except UserNotExistsException as e:
        logger.error(f"Не удалось обновить колоду user_id:{user_id}, deck_id:{deck_id}, deck.name:{deck.name} {e}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "User with such id does not exist")
    except DeckNotExistsException as e:
        logger.error(f"Не удалось обновить колоду user_id:{user_id}, deck_id:{deck_id}, deck.name:{deck.name} {e}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Deck with such id does not exist")
    else:
        logger.success(f"Обновил колоду deck_id:{deck_id}, user_id:{user_id}, deck.name:{deck.name}")
        return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.delete("/{deck_id:int}", status_code=status.HTTP_204_NO_CONTENT, responses=user_deck_responses)
async def delete_deck(deck_id: int, user_id: int = Header(...), decks_dal: DecksDAL = Depends(get_decks_dal)):
    """
    Удаление колоды
    """
    try:
        await decks_dal.delete_deck(deck_id, user_id)
    except UserNotExistsException as e:
        logger.error(f"Не удалось удалить колоду user_id:{user_id}, deck_id:{deck_id} {e}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "User with such id does not exist")
    except DeckNotExistsException as e:
        logger.error(f"Не удалось удалить колоду user_id:{user_id}, deck_id:{deck_id} {e}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Deck with such id does not exist")
    else:
        logger.success(f"Удалил колоду deck_id:{deck_id}, user_id:{user_id}")
        return Response(status_code=status.HTTP_204_NO_CONTENT)
