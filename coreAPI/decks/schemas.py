from datetime import datetime

from pydantic import BaseModel, Field


class DeckName(BaseModel):
    name: str


class DeckResponse(DeckName):
    id: int
    created_at: datetime

    class Config:
        orm_mode = True


class StudyFlashcard(BaseModel):
    id: int
    front_side: str
    back_side: str
    comment: str | None

    class Config:
        orm_mode = True


class StudyRecord(BaseModel):
    deck_id: int
    flashcard_id: int
    record: str = Field(..., description="'remember' or 'forget'")
    timestamp: datetime | None = None
