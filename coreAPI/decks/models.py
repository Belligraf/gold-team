from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, func

from config import Base


class Deck(Base):
    __tablename__ = "decks"

    id = Column(Integer, autoincrement=True, primary_key=True)
    user_id = Column(Integer, ForeignKey("users.id", ondelete="CASCADE"), autoincrement=False)
    name = Column(String(150), nullable=False)
    created_at = Column(DateTime, nullable=False, server_default=func.now())
