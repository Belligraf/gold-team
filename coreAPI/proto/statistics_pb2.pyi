from google.protobuf import timestamp_pb2 as _timestamp_pb2
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class StatisticActionMessage(_message.Message):
    __slots__ = ["Action", "DeckId", "FlashcardId", "Timestamp", "UserId"]
    ACTION_FIELD_NUMBER: _ClassVar[int]
    Action: str
    DECKID_FIELD_NUMBER: _ClassVar[int]
    DeckId: int
    FLASHCARDID_FIELD_NUMBER: _ClassVar[int]
    FlashcardId: int
    TIMESTAMP_FIELD_NUMBER: _ClassVar[int]
    Timestamp: _timestamp_pb2.Timestamp
    USERID_FIELD_NUMBER: _ClassVar[int]
    UserId: int
    def __init__(self, UserId: _Optional[int] = ..., DeckId: _Optional[int] = ..., FlashcardId: _Optional[int] = ..., Action: _Optional[str] = ..., Timestamp: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...
