from sqlalchemy import Column, BigInteger, Integer, String, Text, ForeignKey, DateTime, func, SmallInteger

from config import Base


class Flashcard(Base):
    __tablename__ = "flashcards"

    id = Column(BigInteger, autoincrement=True, primary_key=True)
    front_side = Column(String(300), nullable=False)
    back_side = Column(String(300), nullable=False)
    comment = Column(Text)
    created_at = Column(DateTime, nullable=False, server_default=func.now())
    level = Column(SmallInteger, nullable=False, default=0)
    next_appearance = Column(DateTime, nullable=False, server_default=func.now())


class DeckFlashcard(Base):
    __tablename__ = "decks_flashcards"

    deck_id = Column(Integer, ForeignKey("decks.id", ondelete="CASCADE"),
                     autoincrement=False, primary_key=True)
    flashcard_id = Column(BigInteger, ForeignKey("flashcards.id", ondelete="CASCADE"),
                          autoincrement=False, primary_key=True)


class SharedDeckFlashcard(Base):
    __tablename__ = "shared_decks_flashcards"

    deck_id = Column(Integer, ForeignKey("shared_decks.id", ondelete="CASCADE"),
                     autoincrement=False, primary_key=True)
    flashcard_id = Column(BigInteger, ForeignKey("flashcards.id", ondelete="CASCADE"),
                          autoincrement=False, primary_key=True)
