class DeckNotExistsException(Exception):
    pass


class FlashcardNotExistsException(Exception):
    pass
