from datetime import datetime

from pydantic import BaseModel, Field


class FlashcardBase(BaseModel):
    front_side: str
    back_side: str
    comment: str | None = None


class FlashcardCreate(FlashcardBase):
    deck_id: int
    is_shared_deck_flashcard: bool = False


class FlashcardResponse(FlashcardBase):
    id: int
    created_at: datetime | None = Field(None, description="Only presented for regular deck")
    level: int | None = Field(None, description="Only presented for regular deck")
    next_appearance: datetime | None = Field(None, description="Only presented for regular deck")

    class Config:
        orm_mode = True


class FlashcardUpdate(BaseModel):
    front_side: str | None = None
    back_side: str | None = None
    comment: str | None = None
    deck_id: int | None = None
