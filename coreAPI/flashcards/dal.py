from datetime import datetime

from sqlalchemy import update, delete
from sqlalchemy.future import select
from sqlalchemy.orm import Session

from config import async_session, kafka_producer
from decks import Deck
from flashcards import Flashcard, DeckFlashcard, SharedDeckFlashcard
from flashcards.schemas import FlashcardCreate, FlashcardResponse, FlashcardUpdate
from flashcards.services import DeckNotExistsException, FlashcardNotExistsException
from shared_decks import SharedDeck


class FlashcardsDAL:
    def __init__(self, db_session: Session):
        self.db_session = db_session

    async def _get_all_decks_ids_for_user(self, user_id: int) -> list[int]:
        result = await self.db_session.execute(select(Deck.id).where(Deck.user_id == user_id))
        return result.scalars().all()

    async def _get_all_shared_decks_ids_for_user(self, user_id: int) -> list[int]:
        result = await self.db_session.execute(select(SharedDeck.id).where(SharedDeck.user_id == user_id))
        return result.scalars().all()

    async def _get_all_flashcards_ids_for_user(self, user_id: int) -> list[int]:
        return (await self._get_all_decks_flashcards_ids_for_user(user_id)) + \
               (await self._get_all_shared_decks_flashcards_ids_for_user(user_id))

    async def _get_all_decks_flashcards_ids_for_user(self, user_id: int) -> list[int]:
        result = await self.db_session.execute(
            select(DeckFlashcard.flashcard_id).join(Deck).where(Deck.user_id == user_id)
        )
        return result.scalars().all()

    async def _get_all_shared_decks_flashcards_ids_for_user(self, user_id: int) -> list[int]:
        result = await self.db_session.execute(
            select(SharedDeckFlashcard.flashcard_id).join(SharedDeck).where(SharedDeck.user_id == user_id)
        )
        return result.scalars().all()

    async def _update_shared_deck(self, shared_deck_id: int):
        await self.db_session.execute(
            update(
                SharedDeck
            ).where(
                SharedDeck.id == shared_deck_id
            ).values(
                updated_at=datetime.now()
            )
        )

    async def get_deck_flashcards(self, deck_id: int, user_id: int) -> list[FlashcardResponse] | None:
        if deck_id not in await self._get_all_decks_ids_for_user(user_id):
            return None

        result = await self.db_session.execute(
            select(
                Flashcard
            ).join(
                DeckFlashcard
            ).where(
                DeckFlashcard.deck_id == deck_id
            ).order_by(
                Flashcard.created_at
            )
        )
        flashcards = result.scalars().all()
        flashcard_responses = []
        for flashcard in flashcards:
            flashcard_responses.append(FlashcardResponse(
                deck_id=deck_id,
                front_side=flashcard.front_side,
                back_side=flashcard.back_side,
                comment=flashcard.comment,
                id=flashcard.id,
                created_at=flashcard.created_at,
                level=flashcard.level,
                next_appearance=flashcard.next_appearance
            ))
        return flashcard_responses

    async def get_shared_deck_flashcards(self, shared_deck_id: int, user_id: int) -> list[FlashcardResponse] | None:
        if shared_deck_id not in await self._get_all_shared_decks_ids_for_user(user_id):
            return None

        result = await self.db_session.execute(
            select(
                Flashcard
            ).join(
                SharedDeckFlashcard
            ).where(
                SharedDeckFlashcard.deck_id == shared_deck_id
            ).order_by(
                Flashcard.created_at
            )
        )
        flashcards = result.scalars().all()
        shared_flashcard_responses = []
        for flashcard in flashcards:
            shared_flashcard_responses.append(FlashcardResponse(
                deck_id=shared_deck_id,
                front_side=flashcard.front_side,
                back_side=flashcard.back_side,
                comment=flashcard.comment,
                id=flashcard.id
            ))
        return shared_flashcard_responses

    async def create_deck_flashcard(self, flashcard: FlashcardCreate, user_id: int) -> Flashcard:
        if flashcard.deck_id not in await self._get_all_decks_ids_for_user(user_id):
            raise DeckNotExistsException()

        new_flashcard = Flashcard(
            front_side=flashcard.front_side,
            back_side=flashcard.back_side,
            comment=flashcard.comment
        )
        self.db_session.add(new_flashcard)
        await self.db_session.flush()
        await self.db_session.refresh(new_flashcard)
        new_deck_flashcard = DeckFlashcard(
            deck_id=flashcard.deck_id,
            flashcard_id=new_flashcard.id
        )
        self.db_session.add(new_deck_flashcard)
        await self.db_session.flush()

        result = await self.db_session.execute(select(Deck).where(Deck.id == flashcard.deck_id))
        deck = result.scalars().first()
        await kafka_producer.send_statistics_message(deck.user_id, deck.id, new_flashcard.id, "create", datetime.now())

        return new_flashcard

    async def create_shared_deck_flashcard(self, flashcard: FlashcardCreate, user_id: int) -> Flashcard:
        if flashcard.deck_id not in await self._get_all_shared_decks_ids_for_user(user_id):
            raise DeckNotExistsException()

        new_flashcard = Flashcard(
            front_side=flashcard.front_side,
            back_side=flashcard.back_side,
            comment=flashcard.comment
        )
        self.db_session.add(new_flashcard)
        await self.db_session.flush()
        await self.db_session.refresh(new_flashcard)
        new_deck_flashcard = SharedDeckFlashcard(
            deck_id=flashcard.deck_id,
            flashcard_id=new_flashcard.id
        )
        self.db_session.add(new_deck_flashcard)
        await self._update_shared_deck(flashcard.deck_id)
        await self.db_session.flush()
        return new_flashcard

    async def update_deck_flashcard(self, flashcard_id: int, flashcard: FlashcardUpdate, user_id: int):
        if flashcard_id in await self._get_all_decks_flashcards_ids_for_user(user_id):
            if flashcard.deck_id is not None and flashcard.deck_id in await self._get_all_decks_ids_for_user(user_id):
                await self.db_session.execute(
                    update(
                        DeckFlashcard
                    ).where(
                        DeckFlashcard.flashcard_id == flashcard_id
                    ).values(
                        deck_id=flashcard.deck_id
                    )
                )
        elif flashcard_id in await self._get_all_shared_decks_flashcards_ids_for_user(user_id):
            deck_id = flashcard.deck_id
            if deck_id is not None and deck_id in await self._get_all_shared_decks_ids_for_user(user_id):
                result = await self.db_session.execute(
                    select(SharedDeckFlashcard.deck_id).where(SharedDeckFlashcard.flashcard_id == flashcard_id)
                )
                shared_deck_id = result.scalars().first()
                await self.db_session.execute(
                    update(
                        SharedDeckFlashcard
                    ).where(
                        SharedDeckFlashcard.flashcard_id == flashcard_id
                    ).values(
                        deck_id=flashcard.deck_id
                    )
                )
                await self._update_shared_deck(shared_deck_id)
        else:
            raise FlashcardNotExistsException()

        values = flashcard.dict(exclude_none=True, exclude={"deck_id"})
        if len(values) == 0:
            return

        await self.db_session.execute(
            update(
                Flashcard
            ).where(
                Flashcard.id == flashcard_id
            ).values(
                **values
            )
        )
        if flashcard_id in await self._get_all_shared_decks_flashcards_ids_for_user(user_id):
            result = await self.db_session.execute(
                select(SharedDeckFlashcard.deck_id).where(SharedDeckFlashcard.flashcard_id == flashcard_id)
            )
            shared_deck_id = result.scalars().first()
            await self._update_shared_deck(shared_deck_id)
        await self.db_session.flush()

    async def delete_flashcard(self, flashcard_id: int, user_id: int):
        if flashcard_id not in await self._get_all_flashcards_ids_for_user(user_id):
            raise FlashcardNotExistsException()

        await self.db_session.execute(
            delete(Flashcard).where(Flashcard.id == flashcard_id)
        )
        await self.db_session.flush()


async def get_flashcards_dal():
    async with async_session() as session:
        async with session.begin():
            yield FlashcardsDAL(session)
