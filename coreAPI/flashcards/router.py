from fastapi import APIRouter, Depends, HTTPException, status, Response, Header
from loguru import logger
from flashcards.dal import FlashcardsDAL, get_flashcards_dal
from flashcards.schemas import FlashcardResponse, FlashcardCreate, FlashcardUpdate
from flashcards.services import DeckNotExistsException, FlashcardNotExistsException

router = APIRouter(prefix="/flashcards", tags=["Flashcards"])

post_responses = {
    status.HTTP_404_NOT_FOUND: {
        "content": {
            "application/json": {
                "example": {
                    "detail": "Deck with such id does not exist"
                }
            }
        }
    }
}
BAD_REQUEST_ANSWER = "Don't provide both deck_id and shared_deck_id query parameters. Strictly one of them is required."
get_responses = post_responses | {
    status.HTTP_400_BAD_REQUEST: {
        "content": {
            "application/json": {
                "example": {
                    "detail": BAD_REQUEST_ANSWER
                }
            }
        }
    }
}
patch_responses = delete_responses = {
    status.HTTP_404_NOT_FOUND: {
        "content": {
            "application/json": {
                "example": {
                    "detail": "Flashcard with such id does not exist"
                }
            }
        }
    }
}


@router.get("", response_model=list[FlashcardResponse], responses=get_responses)
async def get_flashcards_in_deck(
        deck_id: int | None = None,
        shared_deck_id: int | None = None,
        user_id: int = Header(...),
        flashcards_dal: FlashcardsDAL = Depends(get_flashcards_dal)
):
    """
    Получить карточки в колоде
    """
    if deck_id and shared_deck_id:
        logger.error(f"Не смог получить список карточек из-за того, что указаны оба параметра вместо одного")
        raise HTTPException(status.HTTP_400_BAD_REQUEST, BAD_REQUEST_ANSWER)
    if deck_id is None and shared_deck_id is None:
        logger.error(f"Не смог получить список карточек из-за того, что оба параметра - None")
        raise HTTPException(status.HTTP_400_BAD_REQUEST, BAD_REQUEST_ANSWER)
    if deck_id:
        flashcards = await flashcards_dal.get_deck_flashcards(deck_id, user_id)
    else:
        flashcards = await flashcards_dal.get_shared_deck_flashcards(shared_deck_id, user_id)
    if flashcards is not None:
        logger.success(f"Получил список карточек {flashcards}")
        return flashcards
    else:
        logger.error(f"Не смог получить список карточек: такой колоды не существует")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Deck with such id does not exist")


@router.post("", response_model=FlashcardResponse, status_code=status.HTTP_201_CREATED, responses=post_responses)
async def create_flashcard(
        flashcard: FlashcardCreate,
        user_id: int = Header(...),
        flashcards_dal: FlashcardsDAL = Depends(get_flashcards_dal)
):
    """
    Создать карточку
    """
    try:
        if flashcard.is_shared_deck_flashcard:
            new_flashcard = await flashcards_dal.create_shared_deck_flashcard(flashcard, user_id)
        else:
            new_flashcard = await flashcards_dal.create_deck_flashcard(flashcard, user_id)
    except DeckNotExistsException:
        logger.error(
            f"Не удалось создать карточку, такой колоды не существует user_id:{user_id}, flashcard:{flashcard}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Deck with such id does not exist")
    else:
        logger.success(f"Создал карточку {flashcard}, user_id:{user_id}")
        return new_flashcard


@router.patch("/{flashcard_id:int}", status_code=status.HTTP_204_NO_CONTENT, responses=patch_responses)
async def update_flashcard(
        flashcard_id: int,
        flashcard: FlashcardUpdate,
        user_id: int = Header(...),
        flashcards_dal: FlashcardsDAL = Depends(get_flashcards_dal)
):
    """
    Обновление (изменение) карточки
    """
    try:
        await flashcards_dal.update_deck_flashcard(flashcard_id, flashcard, user_id)
    except FlashcardNotExistsException as e:
        logger.error(f"Не смог обновить карточку flashcard_id:{flashcard_id}, user_id:{user_id}, flashcard:{flashcard}"
                     f" {e}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Flashcard with such id does not exist")
    else:
        logger.success(f"Обновил карточку flashcard_id:{flashcard_id}, user_id:{user_id}, flashcard:{flashcard}")
        return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.delete("/{flashcard_id:int}", status_code=status.HTTP_204_NO_CONTENT, responses=delete_responses)
async def delete_flashcard(
        flashcard_id: int,
        user_id: int = Header(...),
        flashcards_dal: FlashcardsDAL = Depends(get_flashcards_dal)
):
    """
    Удаление карточки
    """
    try:
        await flashcards_dal.delete_flashcard(flashcard_id, user_id)
    except FlashcardNotExistsException as e:
        logger.error(f"Не смог удалить карточку flashcard_id:{flashcard_id}, user_id:{user_id}")
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Flashcard with such id does not exist")
    else:
        logger.success(f"Удалил карточку flashcard_id:{flashcard_id}, user_id:{user_id}")
        return Response(status_code=status.HTTP_204_NO_CONTENT)
