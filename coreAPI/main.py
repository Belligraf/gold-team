import asyncio
import json

import uvicorn
from fastapi import FastAPI
from starlette.middleware.sessions import SessionMiddleware
from starlette.responses import RedirectResponse, JSONResponse

import decks
import flashcards
import shared_decks
from config import kafka_producer, AUTH_SECRET, KAFKA_ACCOUNT_TOPIC_NAME, KAFKA_BOOTSTRAP_SERVER, PORT
from kafka_consumer import KafkaConsumer

kafka_consumer = KafkaConsumer(KAFKA_ACCOUNT_TOPIC_NAME, KAFKA_BOOTSTRAP_SERVER)


def get_application() -> FastAPI:
    application = FastAPI()

    # удаляем /openapi.json эндпоинт из недр FastAPI для переопределения
    for i in range(len(application.routes)):
        if application.routes[i].path == application.openapi_url:
            application.routes.remove(application.routes[i])
            break

    @application.get("/openapi.json", include_in_schema=False)
    async def openapi_json():
        with open("openapi.json") as file:
            return JSONResponse(json.loads(file.read()))

    @application.on_event("startup")
    async def startup():
        asyncio.create_task(kafka_consumer.start())
        asyncio.create_task(kafka_producer.start())

    @application.on_event("shutdown")
    async def shutdown():
        asyncio.create_task(kafka_consumer.stop())
        asyncio.create_task(kafka_producer.stop())

    @application.get("/", include_in_schema=False)
    async def home():
        return RedirectResponse("/docs")

    application.add_middleware(SessionMiddleware, secret_key=AUTH_SECRET)
    application.include_router(decks.router)
    application.include_router(flashcards.router)
    application.include_router(shared_decks.router)
    return application


app = get_application()

if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=PORT)
