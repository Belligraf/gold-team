from datetime import datetime

from loguru import logger
from aiokafka import AIOKafkaProducer

from proto.statistics_pb2 import StatisticActionMessage


class KafkaProducer:
    def __init__(self, topic: str, host: str):
        self.topic = topic
        self.host = host
        self.producer: AIOKafkaProducer | None = None

    async def _init_producer(self):
        logger.info(
            f"Инициализирую - kafka producer, host: {self.host}, topic: {self.topic}, producer: {self.producer}")
        if self.producer is None:
            self.producer = AIOKafkaProducer(bootstrap_servers=self.host)

    async def _send(self, msg: StatisticActionMessage):
        try:
            logger.info(f"Отправляю сообщение - kafka producer, message: {msg}")
            await self.producer.send_and_wait(self.topic, msg.SerializeToString())
        except Exception as e:
            logger.error(f"Не смог отправить сообщение - kafka producer - {e}")

    async def start(self):
        logger.info(f"Стартую - kafka producer")
        await self._init_producer()
        await self.producer.start()

    async def stop(self):
        logger.info(f"Останавливаю - kafka producer")
        if self.producer is not None:
            await self.producer.stop()

    async def send_statistics_message(self, user_id: int, deck_id: int, flashcard_id: int, action: str,
                                      timestamp: datetime):
        logger.info(f"Отправляю статистическое сообщение - kafka producer")
        statistic_record = StatisticActionMessage()
        statistic_record.UserId = user_id
        statistic_record.DeckId = deck_id
        statistic_record.FlashcardId = flashcard_id
        statistic_record.Action = action
        statistic_record.Timestamp.FromDatetime(timestamp)
        await self._send(statistic_record)
