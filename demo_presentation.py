import requests
from loguru import logger

BASE_URL = "http://127.0.0.1:8000"


def register(email, password, username):
    url = f"{BASE_URL}/auth/signup"
    data = {
        "email": email,
        "password": password,
        "username": username
    }
    response = requests.post(url, json=data)
    logger.info(response.status_code)


def login(username, password):
    url = f"{BASE_URL}/auth/get_token"
    data = {
        "password": password,
        "username": username
    }
    response = requests.post(url, json=data)
    logger.info(response.json())
    return response.json()['token']


def create_deck(token, name):
    url = f"{BASE_URL}/decks"
    headers = {
        "Authorization": f"Bearer {token}"
    }
    data = {
        "name": name
    }
    response = requests.post(url, headers=headers, json=data)
    logger.info(response.json())
    return response.json()['id']


def get_decks(token):
    url = f"{BASE_URL}/decks"
    headers = {
        "Authorization": f"Bearer {token}"
    }
    response = requests.get(url, headers=headers)
    logger.info(response.json())


def update_deck(deck_id, token, new_name):
    url = f"{BASE_URL}/decks/{deck_id}"
    headers = {
        "Authorization": f"Bearer {token}"
    }
    data = {
        "deck_id": deck_id,
        "name": new_name
    }
    response = requests.patch(url, json=data, headers=headers)
    logger.info(response.status_code)


def delete_deck(deck_id, token):
    url = f"{BASE_URL}/decks/{deck_id}"
    param = {
        "deck_id": deck_id,
    }
    headers = {
        "Authorization": f"Bearer {token}"
    }
    response = requests.delete(url, params=param, headers=headers)
    logger.info(response.status_code)


def delete_shared_deck(deck_id, token):
    url = f"{BASE_URL}/shared_decks/{deck_id}"
    param = {
        "deck_id": deck_id,
    }
    headers = {
        "Authorization": f"Bearer {token}"
    }
    response = requests.delete(url, params=param, headers=headers)
    logger.info(response.status_code)


def create_study_record(token, deck_id, flashcard_id, record):
    url = f"{BASE_URL}/decks/study"
    headers = {
        "Authorization": f"Bearer {token}"
    }
    data = {
        "deck_id": deck_id,
        "flashcard_id": flashcard_id,
        "record": record,
        "timestamp": "2023-05-18T06:35:18.051"
    }
    response = requests.post(url, headers=headers, json=data)
    logger.info(response.status_code)


def get_flashcards_remember(token, deck_id):
    url = f"{BASE_URL}/decks/study"
    headers = {
        "Authorization": f"Bearer {token}"
    }
    param = {
        "deck_id": deck_id
    }
    response = requests.get(url, params=param, headers=headers)
    logger.info(response.json())


def create_flashcard(token, deck_id, front_side, back_side, comment, is_shared_deck_flashcard):
    url = f"{BASE_URL}/flashcards"
    headers = {
        "Authorization": f"Bearer {token}"
    }
    data = {
        "front_side": front_side,
        "back_side": back_side,
        "comment": comment,
        "deck_id": deck_id,
        "is_shared_deck_flashcard": is_shared_deck_flashcard
    }
    response = requests.post(url, headers=headers, json=data)
    logger.info(response.json())
    return response.json()['id']


def get_flashcards(deck_id, shared_deck_id, token):
    url = f"{BASE_URL}/flashcards"
    if shared_deck_id == "":
        param = {
            "deck_id": deck_id,
        }
    else:
        param = {
            "shared_deck_id": shared_deck_id,
        }
    headers = {
        "Authorization": f"Bearer {token}"
    }
    response = requests.get(url, headers=headers, params=param)
    logger.info(response.json())


def update_flashcard(flashcard_id, deck_id, front_side, back_side, comment, token):
    url = f"{BASE_URL}/flashcards/{flashcard_id}"
    param = {
        "flashcard_id": flashcard_id,
    }
    data = {
        "front_side": front_side,
        "back_side": back_side,
        "comment": comment,
        "deck_id": deck_id
    }
    headers = {
        "Authorization": f"Bearer {token}"
    }
    response = requests.patch(url, json=data, headers=headers, params=param)
    logger.info(response.status_code)


def delete_flashcard(flashcard_id, token):
    url = f"{BASE_URL}/flashcards/{flashcard_id}"
    param = {
        "flashcard_id": flashcard_id,
    }
    headers = {
        "Authorization": f"Bearer {token}"
    }
    response = requests.delete(url, params=param, headers=headers)
    logger.info(response.status_code)


def create_shared_deck(name, description, token):
    url = f"{BASE_URL}/shared_decks"
    headers = {
        "Authorization": f"Bearer {token}"
    }
    data = {
        "name": name,
        "description": description,
    }
    response = requests.post(url, json=data, headers=headers)
    logger.info(response.json())
    return response.json()['id']


def get_shared_deck(author_user_id, name, token):
    url = f"{BASE_URL}/shared_decks"
    headers = {
        "Authorization": f"Bearer {token}"
    }
    data = {
        "name": name,
        "author_user_id": author_user_id
    }
    response = requests.get(url, json=data, headers=headers)
    logger.info(response.json())


def get_shared_deck_from_desk_id(shared_deck_id, token):
    url = f"{BASE_URL}/shared_decks/{shared_deck_id}"
    headers = {
        "Authorization": f"Bearer {token}"
    }
    response = requests.get(url, headers=headers)
    logger.info(response.json())


def update_shared_deck(deck_id, token, name, description):
    url = f"{BASE_URL}/shared_decks/{deck_id}"
    headers = {
        "Authorization": f"Bearer {token}"
    }
    param = {
        "deck_id": deck_id,
    }
    data = {
        "name": name,
        "description": description,
    }
    response = requests.patch(url, json=data, headers=headers, params=param)
    logger.info(response.status_code)


def create_review(token, deck_id, is_positive, comment):
    url = f"{BASE_URL}/shared_decks/review"
    headers = {
        "Authorization": f"Bearer {token}"
    }
    data = {
        "deck_id": deck_id,
        "is_positive": is_positive,
        "comment": comment,
    }
    response = requests.post(url, headers=headers, json=data)
    logger.info(response.status_code)


def create_deck_from_shared_deck(shared_deck_id, token):
    url = f"{BASE_URL}/decks/from_shared_deck"
    param = {
        "shared_deck_id": shared_deck_id,
    }
    headers = {
        "Authorization": f"Bearer {token}"
    }
    response = requests.post(url, params=param, headers=headers)
    logger.info(response.status_code)


def get_number_learned_and_forgotten_cards(token, deck_id, from_date, to_date):
    url = f"{BASE_URL}/statistics/flashcards_days"
    data = {
        "deck_id": deck_id,
        "from_date": from_date,
        "to_date": to_date,
    }
    headers = {
        "Authorization": f"Bearer {token}"
    }
    response = requests.get(url, json=data, headers=headers)
    logger.info(response.json())


def overall_hours(token):
    url = f"{BASE_URL}/statistics/overall_hours"
    headers = {
        "Authorization": f"Bearer {token}"
    }
    response = requests.get(url, headers=headers)
    logger.info(response.json())


def added_flashcards_days(token, deck_id, from_date, to_date):
    url = f"{BASE_URL}/statistics/added_flashcards_days"
    data = {
        "deck_id": deck_id,
        "from_date": from_date,
        "to_date": to_date,
    }
    headers = {
        "Authorization": f"Bearer {token}"
    }
    response = requests.get(url, json=data, headers=headers)
    logger.info(response.json())


def main():
    # Core & Auth

    # 1. Зарегистрироваться
    register(username="patrickredstar", password="123", email="patrickredstar@gmail.com")

    # 2. Войти под пользователем
    token = login(username="patrickredstar", password="123")

    # 4. Создание колоды
    deck_1 = create_deck(token=token, name="my_deck")

    # Создание второй колоды
    deck_2 = create_deck(token=token, name="my_deck_2")

    # 6. Получение колод
    get_decks(token=token)

    # 7. Обновление колоды
    update_deck(deck_id=deck_1, token=token, new_name="my_update_deck")

    # 8. Получение обновлённой колоды
    get_decks(token=token)

    # 9. Удаление колоды
    delete_deck(deck_id=deck_2, token=token)

    # 10. Получение обновлённой колоды
    get_decks(token=token)

    # 11. Создание учебной записи при НЕ правильном id карточки
    create_study_record(token=token, deck_id=deck_1, flashcard_id=1, record="remember")

    # 12. Создание карточки
    flashcards_1 = create_flashcard(token=token, deck_id=deck_1, front_side="red", back_side="красный", comment="цвет",
                                    is_shared_deck_flashcard=False)

    # 13. Получение карточек
    get_flashcards(deck_id=deck_1, token=token, shared_deck_id="")

    # 14. Обновление карточки
    update_flashcard(flashcard_id=flashcards_1, token=token, deck_id=deck_1, front_side="update_red",
                     back_side="обновлённый красный",
                     comment="цвет")

    # 15. Получение карточек
    get_flashcards(deck_id=deck_1, token=token, shared_deck_id="")

    # 16. Создание учебной записи
    create_study_record(token=token, deck_id=deck_1, flashcard_id=flashcards_1, record="remember")

    # 17. Посмотреть карточки, которые нужно учить сейчас
    get_flashcards_remember(token=token, deck_id=deck_1)

    # 18. Создание карточки
    flashcards_2 = create_flashcard(token=token, deck_id=deck_1, front_side="blue", back_side="голубой", comment="цвет",
                                    is_shared_deck_flashcard=False)

    # 19. Получение карточек
    get_flashcards(deck_id=deck_1, token=token, shared_deck_id="")

    # 20. Посмотреть карточки, которые нужно учить сейчас
    get_flashcards_remember(token=token, deck_id=deck_1)

    # 21. Удаление карточки
    delete_flashcard(flashcard_id=flashcards_1, token=token)

    # 22. Получение карточек
    get_flashcards(deck_id=deck_1, token=token, shared_deck_id="")

    # 23. Создать публичную колоду
    shared_decks_1 = create_shared_deck(name="my_shared_deck", description="svdfgnf", token=token)

    # 24. Получить по названию публичные колоды пользователя, передав user_id
    get_shared_deck(author_user_id=1, name="my_shared_deck", token=token)

    # 25. Получить публичную колоду по её shared_deck_id
    get_shared_deck_from_desk_id(shared_deck_id=shared_decks_1, token=token)

    # 26. Обновить информацию публичной колоды
    update_shared_deck(deck_id=shared_decks_1, token=token, name="my_update_shared_deck",
                       description="update_description")
    # Получить публичную колоду по её shared_deck_id
    get_shared_deck_from_desk_id(shared_deck_id=shared_decks_1, token=token)

    # 27. Регистрация пользователя
    register(username="patrick", password="123", email="patrick@gmail.com")

    # 28. Войти в аккаунт
    token_2 = login(username="patrick", password="123")

    # 29. Создать оценку с комментов от пользователя
    create_review(token=token, deck_id=shared_decks_1, is_positive=True, comment="интересно")

    # 30. Получить публичную колоду по её shared_deck_id
    get_shared_deck_from_desk_id(shared_deck_id=shared_decks_1, token=token)

    # 31. Создать колоду от пользователя на основе публичной колоды
    create_deck_from_shared_deck(shared_deck_id=shared_decks_1, token=token_2)

    # 32. Получение колоды
    get_decks(token=token_2)

    # 33. Войти в аккаунт 1 пользователя
    token = login(username="patrickredstar", password="123")

    # 34. Создать карточку публичной колоды
    create_flashcard(token=token, deck_id=shared_decks_1, front_side="soon", back_side="скоро", comment="скоро",
                     is_shared_deck_flashcard=True)

    # 35. Получить список карточек публичной колоды пользователя
    get_flashcards(deck_id="", shared_deck_id=shared_decks_1, token=token)

    # 36. Удалить публичную колоду по deck_id
    delete_shared_deck(deck_id=shared_decks_1, token=token)

    # 37. Проверить, получив по shared_deck_id публичные колоды
    get_shared_deck_from_desk_id(shared_deck_id=shared_decks_1, token=token)

    # Statistics

    # 1. Получить количество выученных и забытых карт в определенной колоде
    get_number_learned_and_forgotten_cards(token=token, deck_id=deck_1, from_date="2023.05.18", to_date="2023.05.18")

    # 2. Получить соотношение количества выученных карточек к количеству забытых в виде почасовой разбивки
    overall_hours(token=token)

    # 3. Получить количество добавленных карт в определенной колоде
    added_flashcards_days(token=token, deck_id=deck_1, from_date="2023.05.18", to_date="2023.05.18")


if __name__ == '__main__':
    main()
