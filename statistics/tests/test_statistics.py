import pytest
from httpx import AsyncClient


@pytest.mark.parametrize("input_data,expected_status,error,expected_data",
                         [
                             ({"deck_id": 1, "user-id": "1"}, 200, '', {'year': 2023}),
                             (
                                     {"deck_id": 1, "user-id": "1", "from_date_string": "2022.11.12"}, 200, '',
                                     {'year': 2023}),
                             ({"deck_id": 1, "user-id": "1", "from_date_string": "202dwa3.11.12"}, 400,
                              "Invalid date argument(s) format, must be yyyy.mm.dd", {'year': 2023}),
                             (
                                     {"deck_id": 1, "user-id": "1", "to_date_string": "2023.11.12"}, 200, '',
                                     {'year': 2023}),
                             ({"deck_id": 1, "user-id": "1", "to_date_string": "202dwa3.11.12"}, 400,
                              "Invalid date argument(s) format, must be yyyy.mm.dd", {'year': 2023})
                         ])
@pytest.mark.asyncio
async def test_get_flashcards_status_statistic(ac: AsyncClient, input_data, expected_status, error, expected_data,
                                        create_model):
    response = await ac.get("/statistics/flashcards_days", headers={"user-id": input_data["user-id"]},
                            params=input_data)
    assert response.status_code == expected_status
    if error and isinstance(response.json()['detail'], list):
        assert response.json()['detail'][0]['msg'] == error
    elif error:
        assert response.json()['detail'] == error
    else:
        assert isinstance(response.json(), list)
        assert response.json()[0]['year'] == expected_data['year']


@pytest.mark.parametrize("input_data,expected_status,error,expected_data",
                         [
                             ({"user-id": "1"}, 200, '',
                              {'hour_range': '11:00~12:00', 'remembers_count': 5, 'forgets_count': 5, })
                         ])
@pytest.mark.asyncio
async def test_get_hour_statistic(ac: AsyncClient, input_data, expected_status, error, expected_data):
    response = await ac.get("/statistics/overall_hours", headers={"user-id": input_data["user-id"]})
    assert response.status_code == expected_status
    if error and isinstance(response.json()['detail'], list):
        assert response.json()['detail'][0]['msg'] == error
    elif error:
        assert response.json()['detail'] == error
    else:
        assert response.json()[0].keys() == expected_data.keys()

@pytest.mark.parametrize("input_data,expected_status,error,expected_data",
                         [
                             ({"deck_id": 1, "user-id": "1"}, 200, '', {'count': 10, 'day': 20, 'month': 5, 'year': 2023}),
                             (
                                     {"deck_id": 1, "user-id": "1", "from_date_string": "2022.11.12"}, 200, '',
                                     {'count': 10, 'day': 20, 'month': 5, 'year': 2023}),
                             ({"deck_id": 1, "user-id": "1", "from_date_string": "202dwa3.11.12"}, 400,
                              "Invalid date argument(s) format, must be yyyy.mm.dd", {'count': 10, 'day': 20, 'month': 5, 'year': 2023}),
                             (
                                     {"deck_id": 1, "user-id": "1", "to_date_string": "2023.11.12"}, 200, '',
                                     {'count': 10, 'day': 20, 'month': 5, 'year': 2023}),
                             ({"deck_id": 1, "user-id": "1", "to_date_string": "202dwa3.11.12"}, 400,
                              "Invalid date argument(s) format, must be yyyy.mm.dd", {'count': 10, 'day': 20, 'month': 5, 'year': 2023})
                         ])
@pytest.mark.asyncio
async def test_get_flashcards_day_statistic(ac: AsyncClient, input_data, expected_status, error, expected_data):
    response = await ac.get("/statistics/added_flashcards_days", headers={"user-id": input_data["user-id"]},
                            params=input_data)
    assert response.status_code == expected_status
    if error and isinstance(response.json()['detail'], list):
        assert response.json()['detail'][0]['msg'] == error
    elif error:
        assert response.json()['detail'] == error
    else:
        assert isinstance(response.json(), list)
        assert response.json()[0].keys() == expected_data.keys()