from aiokafka import AIOKafkaConsumer

from dal import StatisticsDAL
from db_config import async_session
from proto.statistics_pb2 import StatisticActionMessage
from loguru import logger


class KafkaConsumer:
    def __init__(self, topic: str, host: str):
        self.topic = topic
        self.host = host
        self.consumer: AIOKafkaConsumer | None = None
        self.statistics_dal: StatisticsDAL | None = None

    async def _init_services(self):
        logger.info("Инициализирую сервис - kafka consumer")
        if self.consumer is None:
            self.consumer = AIOKafkaConsumer(self.topic, bootstrap_servers=self.host)
        if self.statistics_dal is None:
            async with async_session() as session:
                async with session.begin():
                    self.statistics_dal = StatisticsDAL(session)

    async def start(self):
        await self._init_services()
        await self.consumer.start()
        try:
            logger.info(f"Стартую - kafka consumer")
            async for msg in self.consumer:
                message = StatisticActionMessage()
                message.ParseFromString(msg.value)
                await self.statistics_dal.create_record(message.UserId, message.DeckId, message.FlashcardId,
                                                        message.Action, message.Timestamp.ToDatetime())
        except Exception as e:
            logger.critical(f"Не смог запустить - kafka consumer - {e}")
        finally:
            await self.consumer.stop()

    async def stop(self):
        logger.info(f"Останавливаю - kafka consumer")
        if self.consumer is not None:
            await self.consumer.stop()
