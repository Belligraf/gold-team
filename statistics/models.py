from sqlalchemy import Column, Integer, String, CheckConstraint, DateTime, BigInteger, func

from db_config import Base


class Statistic(Base):
    __tablename__ = "statistics"

    id = Column(BigInteger, autoincrement=True, primary_key=True)
    user_id = Column(Integer, nullable=False, autoincrement=False)
    deck_id = Column(Integer, nullable=False, autoincrement=False)
    flashcard_id = Column(Integer, nullable=False, autoincrement=False)
    action = Column(String(10),
                    CheckConstraint("action in ('remember', 'forget', 'create')",
                                    name="statistics_action_check_constraint"),
                    nullable=False)
    happened_at = Column(DateTime, nullable=False, server_default=func.now())
