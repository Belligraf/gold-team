from datetime import datetime

from models import Statistic
from schemas import FlashcardsDayStatistic, OverallHourStatistic, AddedFlashcardsDayStatistic


class InvalidArgumentException(Exception):
    pass


def split_date_string(date_string: str | None) -> datetime | None:
    if date_string is None:
        return None
    parts = date_string.split(".")
    if len(parts) != 3:
        raise InvalidArgumentException()
    try:
        year, month, day = map(int, parts)
    except ValueError:
        raise InvalidArgumentException()
    else:
        return datetime(year, month, day)


def map_to_flashcards_days_statistics(statistics: list[Statistic]) -> list[FlashcardsDayStatistic]:
    if len(statistics) == 0:
        return []

    flashcards_days_statistics = []
    current_date = statistics[0].happened_at.replace(hour=0, minute=0, second=0, microsecond=0)
    current_remembers_count = 0
    current_forgets_count = 0
    for statistic in statistics:
        if current_date.year == statistic.happened_at.year and \
                current_date.month == statistic.happened_at.month and \
                current_date.day == statistic.happened_at.day:
            pass
        else:
            flashcards_days_statistics.append(FlashcardsDayStatistic(
                year=current_date.year,
                month=current_date.month,
                day=current_date.day,
                remembers_count=current_remembers_count,
                forgets_count=current_forgets_count
            ))
            current_date = statistic.happened_at.replace(hour=0, minute=0, second=0, microsecond=0)
            current_remembers_count = 0
            current_forgets_count = 0
        if statistic.action == "remember":
            current_remembers_count += 1
        else:
            current_forgets_count += 1
    flashcards_days_statistics.append(FlashcardsDayStatistic(
        year=current_date.year,
        month=current_date.month,
        day=current_date.day,
        remembers_count=current_remembers_count,
        forgets_count=current_forgets_count
    ))
    return flashcards_days_statistics


def map_to_overall_hours_statistics(statistics: list[Statistic]) -> list[OverallHourStatistic]:
    if len(statistics) == 0:
        return []

    overall_hours_statistics = []
    current_hour = statistics[0].happened_at.hour
    current_remembers_count = 0
    current_forgets_count = 0
    for statistic in statistics:
        if current_hour == statistic.happened_at.hour:
            pass
        else:
            overall_hours_statistics.append(OverallHourStatistic(
                hour_range=f"{current_hour}:00~{current_hour + 1}:00",
                remembers_count=current_remembers_count,
                forgets_count=current_forgets_count
            ))
            current_hour = statistic.happened_at.hour
            current_remembers_count = 0
            current_forgets_count = 0
        if statistic.action == "remember":
            current_remembers_count += 1
        else:
            current_forgets_count += 1
    overall_hours_statistics.append(OverallHourStatistic(
        hour_range=f"{current_hour}:00~{current_hour + 1}:00",
        remembers_count=current_remembers_count,
        forgets_count=current_forgets_count
    ))
    return overall_hours_statistics


def map_to_added_flashcards_days_statistics(statistics: list[Statistic]) -> list[AddedFlashcardsDayStatistic]:
    if len(statistics) == 0:
        return []

    added_flashcards_days_statistics = []
    current_date = statistics[0].happened_at.replace(hour=0, minute=0, second=0, microsecond=0)
    current_count = 0
    for statistic in statistics:
        if current_date.year == statistic.happened_at.year and \
                current_date.month == statistic.happened_at.month and \
                current_date.day == statistic.happened_at.day:
            pass
        else:
            added_flashcards_days_statistics.append(AddedFlashcardsDayStatistic(
                year=current_date.year,
                month=current_date.month,
                day=current_date.day,
                count=current_count
            ))
            current_date = statistic.happened_at.replace(hour=0, minute=0, second=0, microsecond=0)
            current_count = 0
        current_count += 1
    added_flashcards_days_statistics.append(AddedFlashcardsDayStatistic(
        year=current_date.year,
        month=current_date.month,
        day=current_date.day,
        count=current_count
    ))
    return added_flashcards_days_statistics
