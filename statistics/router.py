from fastapi import APIRouter, status, Depends, HTTPException, Query, Header
from loguru import logger
from dal import StatisticsDAL, get_statistics_dal
from helpers import InvalidArgumentException
from schemas import FlashcardsDayStatistic, OverallHourStatistic, AddedFlashcardsDayStatistic

router = APIRouter(prefix="/statistics", tags=["Statistics"])

invalid_argument_responses = {
    status.HTTP_400_BAD_REQUEST: {
        "content": {
            "application/json": {
                "example": {
                    "detail": "Invalid date argument(s) format, must be yyyy.mm.dd"
                }
            }
        }
    }
}


class DateStringsQueryParams:
    def __init__(
            self,
            from_date_string: str | None = Query(None, description="Format: yyyy.mm.dd"),
            to_date_string: str | None = Query(None, description="Format: yyyy.mm.dd")
    ):
        self.from_date_string = from_date_string
        self.to_date_string = to_date_string


@router.get("/flashcards_days",
            response_model=list[FlashcardsDayStatistic],
            responses=invalid_argument_responses)
async def get_flashcards_days_statistics(
        user_id: int = Header(...),
        deck_id: int | None = None,
        custom_params: DateStringsQueryParams = Depends(),
        statistics_dal: StatisticsDAL = Depends(get_statistics_dal)
):
    """
    Получите количество выученных и забытых карт за определенную колоду и/или период времени
    """
    try:
        statistics = await statistics_dal.get_flashcards_days_statistics(
            user_id,
            custom_params.from_date_string,
            custom_params.to_date_string,
            deck_id
        )
    except InvalidArgumentException:
        logger.error(
            f"Не смог получить статистику за определённый период времени - Invalid Argument - user_id: {user_id}, "
            f"deck_id: {deck_id}, custom_params: {custom_params}")
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "Invalid date argument(s) format, must be yyyy.mm.dd")
    else:
        logger.success(f"Получил статистику за определённый период времени, user_id: {user_id}, deck_id: {deck_id}")
        return statistics


@router.get("/overall_hours", response_model=list[OverallHourStatistic])
async def get_overall_hours_statistics(
        user_id: int = Header(...),
        statistics_dal: StatisticsDAL = Depends(get_statistics_dal)
):
    """
    Получите соотношение количества выученных карточек к количеству забытых в виде почасовой разбивки
    """
    logger.success(
        f"Получил соотношение количества выученных карточек к количеству забытых в виде почасовой разбивки, "
        f"user_id: {user_id}")
    return await statistics_dal.get_overall_hours_statistics(user_id)


@router.get("/added_flashcards_days",
            response_model=list[AddedFlashcardsDayStatistic],
            responses=invalid_argument_responses)
async def get_added_flashcards_days_statistics(
        user_id: int = Header(...),
        deck_id: int | None = None,
        custom_params: DateStringsQueryParams = Depends(),
        statistics_dal: StatisticsDAL = Depends(get_statistics_dal)
):
    """
    Получите количество добавленных карт за определенную колоду и/или период времени
    """
    try:
        statistics = await statistics_dal.get_added_flashcards_days_statistics(
            user_id,
            custom_params.from_date_string,
            custom_params.to_date_string,
            deck_id
        )
    except InvalidArgumentException:
        logger.error(
            f"Не смог количество добавленных карт за определенную колоду и/или период времени - Invalid Argument - "
            f"user_id: {user_id}, deck_id: {deck_id}, custom_params: {custom_params}")
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "Invalid date argument(s) format, must be yyyy.mm.dd")
    else:
        logger.success(
            f"Получил количество добавленных карт за определенную колоду и/или период времени, user_id: {user_id}, "
            f"deck_id: {deck_id}")
        return statistics
