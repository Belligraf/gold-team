from datetime import datetime

from sqlalchemy.future import select
from sqlalchemy.orm import Session

from db_config import async_session
from helpers import split_date_string, map_to_flashcards_days_statistics, \
    map_to_overall_hours_statistics, map_to_added_flashcards_days_statistics
from models import Statistic
from schemas import FlashcardsDayStatistic, OverallHourStatistic, AddedFlashcardsDayStatistic


class StatisticsDAL:
    def __init__(self, db_session: Session):
        self.db_session = db_session
        self.create_action = "create"

    async def create_record(self, user_id: int, deck_id: int, flashcard_id: int, action: str, happened_at: datetime):
        record = Statistic(user_id=user_id, deck_id=deck_id, flashcard_id=flashcard_id, action=action,
                           happened_at=happened_at)
        self.db_session.add(record)
        await self.db_session.commit()

    async def get_flashcards_days_statistics(
            self,
            user_id: int,
            from_date_string: str | None,
            to_date_string: str | None,
            deck_id: int | None
    ) -> list[FlashcardsDayStatistic]:
        from_date = split_date_string(from_date_string)
        to_date = split_date_string(to_date_string)

        request = select(Statistic).where(Statistic.user_id == user_id).where(Statistic.action != self.create_action)
        if deck_id:
            request = request.where(Statistic.deck_id == deck_id)
        if from_date:
            request = request.where(Statistic.happened_at >= from_date)
        if to_date:
            request = request.where(Statistic.happened_at <= to_date)
        request = request.order_by(Statistic.happened_at)

        result = await self.db_session.execute(request)
        statistics = result.scalars().all()
        return map_to_flashcards_days_statistics(statistics)

    async def get_overall_hours_statistics(self, user_id: int) -> list[OverallHourStatistic]:
        result = await self.db_session.execute(
            select(Statistic).where(Statistic.action != self.create_action).where(Statistic.user_id == user_id)
        )
        statistics = result.scalars().all()
        statistics.sort(key=lambda statistic: statistic.happened_at.hour)
        return map_to_overall_hours_statistics(statistics)

    async def get_added_flashcards_days_statistics(
            self,
            user_id: int,
            from_date_string: str | None,
            to_date_string: str | None,
            deck_id: int | None
    ) -> list[AddedFlashcardsDayStatistic]:
        from_date = split_date_string(from_date_string)
        to_date = split_date_string(to_date_string)

        request = select(Statistic).where(Statistic.user_id == user_id).where(Statistic.action == self.create_action)
        if deck_id:
            request = request.where(Statistic.deck_id == deck_id)
        if from_date:
            request = request.where(Statistic.happened_at >= from_date)
        if to_date:
            request = request.where(Statistic.happened_at <= to_date)
        request = request.order_by(Statistic.happened_at)

        result = await self.db_session.execute(request)

        flashcards = result.scalars().all()
        return map_to_added_flashcards_days_statistics(flashcards)


async def get_statistics_dal():
    async with async_session() as session:
        async with session.begin():
            yield StatisticsDAL(session)
