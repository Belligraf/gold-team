from pydantic import BaseModel


class FlashcardsDayStatistic(BaseModel):
    year: int
    month: int
    day: int
    remembers_count: int
    forgets_count: int


class OverallHourStatistic(BaseModel):
    hour_range: str  # 0:00~1:00, 1:00~2:00, ..., 23:00~24:00
    remembers_count: int
    forgets_count: int


class AddedFlashcardsDayStatistic(BaseModel):
    year: int
    month: int
    day: int
    count: int
