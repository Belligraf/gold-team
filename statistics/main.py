import asyncio

import uvicorn
from environs import Env
from fastapi import FastAPI
from starlette.middleware.sessions import SessionMiddleware
from starlette.responses import RedirectResponse

from db_config import engine, Base, PORT
from kafka_consumer import KafkaConsumer
from router import router

env = Env()
env.read_env()

kafka_consumer = KafkaConsumer(env.str("KAFKA_TOPIC_NAME"), env.str("KAFKA_BOOTSTRAP_SERVER"))


def get_application() -> FastAPI:
    application = FastAPI()

    @application.on_event("startup")
    async def startup():
        async with engine.begin() as conn:
            await conn.run_sync(Base.metadata.create_all)
        asyncio.create_task(kafka_consumer.start())

    @application.on_event("shutdown")
    async def shutdown():
        asyncio.create_task(kafka_consumer.stop())

    @application.get("/", include_in_schema=False)
    async def home():
        return RedirectResponse("/docs")

    application.add_middleware(SessionMiddleware, secret_key=env.str("AUTH_SECRET"))
    application.include_router(router)

    return application


app = get_application()

if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=PORT)
