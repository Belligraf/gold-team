package controllers

import (
	"github.com/gin-gonic/gin"
	"goldteam/controllers/schemas"
	"goldteam/initializers"
	"goldteam/service"
	"net/http"
)

func Signup(c *gin.Context) {
	var user schemas.UserSignUp
	if err := c.Bind(&user); err != nil {
		service.NewErrorResponse(c, service.CustomError{StatusCode: 422, VisibleError: err.Error()})
		return
	}
	if err := initializers.UserManager.CreateUser(user); err.Error != nil {
		service.NewErrorResponse(c, err)
		return
	}
	c.Status(http.StatusCreated)
}

func Login(c *gin.Context) {
	var user schemas.UserLogin
	if err := c.Bind(&user); err != nil {
		service.NewErrorResponse(c, service.CustomError{StatusCode: 422, VisibleError: err.Error()})
		return
	}
	token, err := initializers.UserManager.LoginUser(c, user)
	if err != service.NoError {
		service.NewErrorResponse(c, err)
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"token": token,
	})
}

func Delete(c *gin.Context) {
	userId := c.GetHeader("user-id")
	err := initializers.UserManager.DeleteUser(userId)
	if err != service.NoError {
		service.NewErrorResponse(c, err)
		return
	}
	c.Status(http.StatusNoContent)
}

func Patch(c *gin.Context) {
	userId := c.GetHeader("user-id")
	var user schemas.UserPatch
	if err := c.Bind(&user); err != nil {
		service.NewErrorResponse(c, service.CustomError{StatusCode: 422, VisibleError: err.Error()})
		return
	}
	err := initializers.UserManager.PatchUser(userId, user)
	if err != service.NoError {
		service.NewErrorResponse(c, err)
		return
	}
	c.Status(http.StatusNoContent)
}
