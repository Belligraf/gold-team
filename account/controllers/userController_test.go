package controllers

import (
	"bytes"
	"encoding/json"
	"goldteam/controllers/schemas"
	"goldteam/service"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestSignup(t *testing.T) {
	gin.SetMode(gin.TestMode)

	// создаем экземпляр Gin Engine
	r := gin.New()

	// добавляем обработчик Signup к нашему роутеру
	r.POST("/signup", Signup)

	// создаем тестовый запрос
	user := schemas.UserSignUp{
		Username: "testuser",
		Email:    "testuser@example.com",
		Password: "testpassword",
	}
	jsonUser, _ := json.Marshal(user)
	req, _ := http.NewRequest("POST", "/signup", bytes.NewBuffer(jsonUser))
	req.Header.Set("Content-Type", "application/json")
	w := httptest.NewRecorder()

	// отправляем запрос
	r.ServeHTTP(w, req)

	// проверяем ответ
	assert.Equal(t, http.StatusOK, w.Code)
}

//func TestSignup(t *testing.T) {
//	type args struct {
//		c *gin.Context
//	}
//	tests := []struct {
//		name string
//		args args
//	}{
//		// TODO: Add test cases.
//	}
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			Signup(tt.args.c)
//		})
//	}
//}

func TestLogin(t *testing.T) {
	gin.SetMode(gin.TestMode)

	// создаем экземпляр Gin Engine
	r := gin.New()

	// добавляем обработчик Login к нашему роутеру
	r.POST("/login", Login)

	// создаем фиктивный токен
	token := "testtoken"

	// создаем мок сервиса UserService
	userServiceMock := userServiceMock{
		LoginUserFunc: func(c *gin.Context, user schemas.UserLogin) (string, service.CustomError) {
			if user.Username == "testusername" && user.Password == "testpassword" {
				return token, service.NoError
			} else {
				return "", service.CustomError{
					StatusCode:   401,
					VisibleError: "Invalid email or password",
				}
			}
		},
	}

	// устанавливаем мок сервиса UserService в качестве глобальной зависимости
	// initializers.UserService = userServiceMock
	_ = userServiceMock

	// создаем тестовый запрос
	user := schemas.UserLogin{
		Username: "testusername",
		Password: "testpassword",
	}
	jsonUser, _ := json.Marshal(user)
	req, _ := http.NewRequest("POST", "/login", bytes.NewBuffer(jsonUser))
	req.Header.Set("Content-Type", "application/json")
	w := httptest.NewRecorder()

	// отправляем запрос
	r.ServeHTTP(w, req)

	// проверяем ответ
	assert.Equal(t, http.StatusOK, w.Code)
	var response map[string]string
	json.Unmarshal(w.Body.Bytes(), &response)
	assert.Equal(t, token, response["token"])

	// тестируем ошибочный случай
	req, _ = http.NewRequest("POST", "/login", bytes.NewBuffer(jsonUser))
	req.Header.Set("Content-Type", "application/json")
	w = httptest.NewRecorder()
	user.Password = "wrongpassword" // меняем пароль, чтобы получить ошибку
	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusUnauthorized, w.Code)

	// проверяем сообщение об ошибке в ответе
	json.Unmarshal(w.Body.Bytes(), &response)
	assert.Equal(t, "Invalid email or password", response["error"])
}

// создаем мок сервиса UserService для тестирования ошибочного случая
type userServiceMock struct {
	CreateUserFunc func(user schemas.UserSignUp) error
	LoginUserFunc  func(c *gin.Context, user schemas.UserLogin) (string, service.CustomError)
}

func (m *userServiceMock) CreateUser(user schemas.UserSignUp) error {
	if m.CreateUserFunc != nil {
		return m.CreateUserFunc(user)
	}
	return nil
}

func (m *userServiceMock) LoginUser(c *gin.Context, user schemas.UserLogin) (string, service.CustomError) {
	if m.LoginUserFunc != nil {
		return m.LoginUserFunc(c, user)
	}
	return "", service.NoError
}
