package main

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"goldteam/controllers"
	"goldteam/initializers"
	"os"
)

func init() {
	initializers.LoadEnvVariables()
	initializers.ConnectToKafka()
	initializers.InitUserManager()
	initializers.SyncDB()
}

func main() {
	logrus.SetFormatter(new(logrus.JSONFormatter))
	app := gin.Default()
	app.GET("/health", controllers.Health)
	app.POST("/signup", controllers.Signup)
	app.POST("/login", controllers.Login)
	app.DELETE("/user", controllers.Delete)
	app.PATCH("/user", controllers.Patch)
	app.Run("0.0.0.0:" + os.Getenv("PORT"))
}
