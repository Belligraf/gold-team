package initializers

import (
	"context"
	"github.com/segmentio/kafka-go"
	"github.com/sirupsen/logrus"
	"goldteam/service"
	"net"
	"os"
	"strconv"
)

var KafkaService service.KafkaService

func ConnectToKafka() {
	host := os.Getenv("KAFKA_HOST")
	topic := os.Getenv("KAFKA_TOPIC")

	conn, err := kafka.Dial("tcp", host)
	if err != nil {
		logrus.Panic("Failed to connect to kafka -- ", err.Error())
	}

	defer conn.Close()

	controller, err := conn.Controller()
	if err != nil {
		logrus.Panic("Failed to connect to kafka %s", err.Error())
	}

	controllerConn, err := kafka.Dial("tcp", net.JoinHostPort(controller.Host, strconv.Itoa(controller.Port)))
	if err != nil {
		logrus.Panic("Failed to connect to kafka %s", err.Error())
	}

	defer controllerConn.Close()

	topicConfigs := []kafka.TopicConfig{{Topic: topic, NumPartitions: 1, ReplicationFactor: 1}}

	err = controllerConn.CreateTopics(topicConfigs...)
	if err != nil {
		logrus.Panic("Failed to connect to kafka %s", err.Error())
	}

	kafkaWriter := &kafka.Writer{
		Addr:  kafka.TCP(host),
		Topic: topic,
	}

	KafkaService = service.KafkaService{Kafka: kafkaWriter}

	err = kafkaWriter.WriteMessages(context.Background(), kafka.Message{
		Value: []byte("test message"),
	})

	if err != nil {
		logrus.Panic("Failed to connect to kafka %s", err)
	}
}
