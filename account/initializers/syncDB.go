package initializers

import (
	"github.com/sirupsen/logrus"
	"goldteam/models"
)

func SyncDB() {
	err := DB.AutoMigrate(&models.User{})
	if err != nil {
		logrus.Panic("Unsuccessful DB tables initialisation")
	}
}
