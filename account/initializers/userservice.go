package initializers

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"goldteam/service"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"os"
)

var DB *gorm.DB

var UserManager service.UserManager

func InitUserManager() {
	var err error
	var dsn string

	dsn = fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s",
		os.Getenv("DBHOST"),
		os.Getenv("DBUSER"),
		os.Getenv("DBPASS"),
		os.Getenv("DBNAME"),
		os.Getenv("DBPORT"))

	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		logrus.Panic("Failed to connect to db")
	}
	UserManager = service.UserManager{DB: DB, Kafka: KafkaService}
}
